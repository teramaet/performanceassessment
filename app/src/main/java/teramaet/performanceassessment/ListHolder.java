package teramaet.performanceassessment;

import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;


public class ListHolder extends RecyclerView.ViewHolder{

    public TextView titleText;

    public ListHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
