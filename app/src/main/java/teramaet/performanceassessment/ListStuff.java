package teramaet.performanceassessment;


public class ListStuff {

    public String title;
    public String dateadded;
    public String due;
    public String category;


    public ListStuff(String title, String dateadded, String due, String category){
        this.title = title;
        this.dateadded = dateadded;
        this.due = due;
        this.category = category;
    }

}
