package teramaet.performanceassessment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ListFragment extends Fragment{


    private RecyclerView recyclerView;
    private PerformanceAssessment activity;
    private ActivityCallback activityCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (PerformanceAssessment)activity;
        activityCallback = (ActivityCallback)activity;

    }
    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ListStuff listStuff = new ListStuff("Stuff1", "Yesterday", "Tomorrow", "This Project");
        ListStuff listStuff2 = new ListStuff("Stuff5", "2 days ago", "2 days from now", "Stuff");
        ListStuff listStuff3 = new ListStuff("Stuff9", "3 days ago", "3 days from now", "????");
        ListStuff listStuff4 = new ListStuff("Stuff13", "4 days ago", "4 days from now", "!!!!!!");

        activity.listStuff.add(listStuff);
        activity.listStuff.add(listStuff2);
        activity.listStuff.add(listStuff3);
        activity.listStuff.add(listStuff4);

        ListAdapter adapter = new ListAdapter(activity.listStuff, activityCallback);
        recyclerView.setAdapter(adapter);

        return view;
    }




}
