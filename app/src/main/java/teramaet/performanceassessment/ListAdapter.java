package teramaet.performanceassessment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListHolder>  {


    private ArrayList<ListStuff> listStuff;
    private ActivityCallback activityCallback;

    public ListAdapter(ArrayList<ListStuff> ListStuff, ActivityCallback activityCallback ) {
        this.listStuff = ListStuff;
        this.activityCallback = activityCallback;
    }


    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1,parent, false);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(ListHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCallback.onPostSelected(position);
            }
        });
                holder.titleText.setText(listStuff.get(position).title);
        }



    @Override
    public int getItemCount() {
        return listStuff.size();
    }


}
