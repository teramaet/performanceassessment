package teramaet.performanceassessment;

import android.app.FragmentTransaction;
import android.app.Fragment;
import java.util.ArrayList;

public class PerformanceAssessment extends SingleFragmentActivity implements ActivityCallback{

    public ArrayList<ListStuff> listStuff = new ArrayList<>();
    public int currentItem;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        return new ListFragment();
    }

    @Override
    public void onPostSelected(int position) {
        if(findViewById(R.id.detail_fragment_container) == null) {
            currentItem = position;
            Fragment newFragment = new ItemViewFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        else{
            Fragment detailFragment =  new ItemViewFragment();
            getSupportFragmentManager().beginTransaction();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.detail_fragment_container, detailFragment);
            transaction.commit();

    }
}
    }
