package teramaet.performanceassessment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemViewFragment extends Fragment{

    private RecyclerView recyclerView;
    private PerformanceAssessment activity;
    private ActivityCallback activityCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (PerformanceAssessment)activity;
        activityCallback = (ActivityCallback)activity;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_itemview_list, container, false);

        TextView tv1 = (TextView)layoutView.findViewById(R.id.tv1);
        TextView tv2 = (TextView)layoutView.findViewById(R.id.tv2);
        TextView tv3 = (TextView)layoutView.findViewById(R.id.tv3);
        TextView tv4 = (TextView)layoutView.findViewById(R.id.tv4);

        tv1.setText("Title:" + activity.listStuff.get(activity.currentItem).title);
        tv2.setText("Date added:" + activity.listStuff.get(activity.currentItem).dateadded);
        tv3.setText("Due:" + activity.listStuff.get(activity.currentItem).due);
        tv4.setText("Category:" + activity.listStuff.get(activity.currentItem).category);

        return layoutView;
    }
}
